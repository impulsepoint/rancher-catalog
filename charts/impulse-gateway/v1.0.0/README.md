# Impulse Private Gateway

### Info:

This template creates an instance of the Impulse private gateway intended for use with the Impulse SDP product.

### Usage:

Select impulse-gateway from the catalog.

Enter your account name.

Click deploy.

Approve the gateway in the SDP administration UI.